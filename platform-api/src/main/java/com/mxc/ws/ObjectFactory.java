
package com.mxc.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mxc.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetMybalanceResponse_QNAME = new QName("http://ws.mxc.com/", "getMybalanceResponse");
    private final static QName _GetMybalance_QNAME = new QName("http://ws.mxc.com/", "getMybalance");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mxc.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetMybalance }
     * 
     */
    public GetMybalance createGetMybalance() {
        return new GetMybalance();
    }

    /**
     * Create an instance of {@link GetMybalanceResponse }
     * 
     */
    public GetMybalanceResponse createGetMybalanceResponse() {
        return new GetMybalanceResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMybalanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.mxc.com/", name = "getMybalanceResponse")
    public JAXBElement<GetMybalanceResponse> createGetMybalanceResponse(GetMybalanceResponse value) {
        return new JAXBElement<GetMybalanceResponse>(_GetMybalanceResponse_QNAME, GetMybalanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMybalance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.mxc.com/", name = "getMybalance")
    public JAXBElement<GetMybalance> createGetMybalance(GetMybalance value) {
        return new JAXBElement<GetMybalance>(_GetMybalance_QNAME, GetMybalance.class, null, value);
    }

}
