package com.platform.api;

import com.platform.service.MxcService;
import com.platform.util.ApiBaseAction;
import com.platform.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 部门管理Controller
 *
 * @author liepngjun
 * @email 939961241@qq.com
 * @date 2017-09-17 23:58:47
 */
@Api(tags = "梦夏城KTV相关")
@RestController
@RequestMapping("/api/balance")
public class MxcController extends ApiBaseAction {
    @Autowired
    private MxcService mxcService;
    private List<String> list;


    @ApiOperation(value = "获取余额以及剩余酒水")
    @GetMapping("/query")
    public Object list(@RequestParam(value = "mobile", defaultValue = "") String mobile) {
        if(StringUtils.isNotEmpty(mobile))
        {
            try {
                Map<String, Object> params = new HashMap<>();
                params.put("mobile", mobile);
                list = mxcService.getBalance(params);
                return toResponsSuccess(list);
            }catch (SOAPFaultException e)
            {
                e.printStackTrace();
                return toResponsFail("query_err");
            }
        }else{
            return toResponsFail("no_bound_mobile");
        }

    }

}
