package com.platform.service;

import com.mxc.ws.Balance;
import com.platform.util.WebServiceFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 17:59:15
 */
@Service("mxcService")
public class MxcService{
    private List<String> mybalance;


    public List<String> getBalance(Map<String, Object> map) {
//        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]  {"spring-ws.xml"});
//        Balance balance = (Balance) context.getBean("balance");
        Balance balance = (Balance)WebServiceFactory.cacheWebService.get("balance");

        System.out.println("==="+ balance.hashCode());
        mybalance = balance.getMybalance((String)map.get("mobile"));
//        for (String s : mybalance) {
//            System.out.println(s+"=====");
//        }
        return mybalance;
    }

}
