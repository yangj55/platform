package com.platform.util;

import java.util.HashMap;
import java.util.Map;

import com.mxc.ws.Balance;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class WebServiceFactory {

	private static Logger logger = Logger.getLogger(WebServiceFactory.class);

	//定义私有构造方法（防止通过 new WebServiceFactory()去实例化）
	private WebServiceFactory(){

	}

	private static ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]  {"spring-ws.xml"});
	public static Map<String,Object> cacheWebService = new HashMap<String,Object>();

	//把webservcie缓存到cacheWebService中

	public static void create(){
		if(cacheWebService.get("balance") == null) {
			Balance balance = (Balance) context.getBean("balance");
			cacheWebService.put("balance", balance);
		}
	}


}
