package com.platform.controller;

import com.mxc.ws.*;
import com.platform.BaseSpringTestCase;
import com.platform.entity.SysUserEntity;
import com.platform.interceptor.WsClientOutInterceptor;
import com.platform.service.SysUserService;
import com.platform.service.TestSysUserService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.junit.Test;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.List;

/**
 * 会员测试
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2018-07-09 10:13:43
 */
public class TestWs extends BaseSpringTestCase {
    @Autowired
    TestSysUserService testSysUserService;
    @Autowired
    SysUserService sysUserService;
    private Logger logger = getLogger();

    /**
     * 使用测试类
     */

    @Test
    public void test1() {
        BalanceImplService factory = new BalanceImplService();
        Balance balance = factory.getBalanceImplPort();

        Client client = ClientProxy.getClient(balance);
        client.getOutInterceptors().add(new WsClientOutInterceptor("xiaoming", "123456"));

        List<String> list = balance.getMybalance("1241234");
        System.out.println("======="+list.toString());
    }

    @Test
    public void test2() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]  {"spring-ws.xml"});
        Balance balance = (Balance) context.getBean("balance");
        List<String> list = balance.getMybalance("12341234");
        System.out.println("======="+list.toString());
    }

    @Test
    public void test3() {
        System.out.println("======="+Balance.class.getName());
        System.out.println("======="+Balance.class.getSimpleName());
        System.out.println("======="+Balance.class.getTypeName());
    }


}
