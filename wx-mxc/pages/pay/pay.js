var app = getApp();
var util = require('../../utils/util.js');
var api = require('../../config/api.js');

Page({
      data: {
            orderId: 0,
            actualPrice: 0.00
      },
      onLoad: function(options) {
            // 页面初始化 options为页面跳转所带来的参数
            this.setData({
                  //orderId: options.orderId,
                  //actualPrice: options.actualPrice
            });

            if (app.globalData.mobile != null && app.globalData.mobile != "") {

            } else {
                  console.log("需要绑定手机后才可以操作!");
                  //需要绑定手机后才可以操作
                  wx.showModal({
                        title: '',
                        content: '请先绑定手机',
                        success: function(res) {
                              if (res.confirm) {
                                    wx.removeStorageSync("mobile");
                                    wx.navigateTo({
                                          url: '/pages/auth/mobile/mobile'
                                    })
                              } else if (res.cancel) {
                                    //返回上一页
                                    wx.navigateBack({
                                          delta: 1
                                    })
                              }
                        }
                  });
                  return;
            }
      },
      onReady: function() {

      },
      onShow: function() {
            // 页面显示

      },
      onHide: function() {
            // 页面隐藏

      },
      onUnload: function() {
            // 页面关闭

      },
      //向服务请求支付参数
      requestPayParam() {
            let that = this;
            util.request(api.PayPrepayIdMXC, {
                mobile: app.globalData.mobile,
                payType: 1
            }).then(function(res) {
                  if (res.errno === 0) {
                        let payParam = res.data;
                        wx.requestPayment({
                              'timeStamp': payParam.timeStamp,
                              'nonceStr': payParam.timeStamp,
                              'package': payParam.nonceStr,
                              'signType': payParam.signType,
                              'paySign': payParam.paySign,
                              'success': function(res) {
                                    wx.redirectTo({
                                          url: '/pages/payResult/payResult?status=true',
                                    })
                              },
                              'fail': function(res) {
                                    wx.redirectTo({
                                          url: '/pages/payResult/payResult?status=false',
                                    })
                              }
                        })
                  }else{
                      util.showErrorToast(res.errmsg);
                  }
            });
      },
      startPay:function() {
            this.requestPayParam();
      },
      bindCancel: function() {
            wx.navigateBack({})
      },
})