var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var user = require('../../../services/user.js');
var app = getApp();

Page({
    data: {
        userInfo: {},
        hasMobile: ''
    },
    onLoad: function (options) {
        // 页面初始化 options为页面跳转所带来的参数
    },
    onReady: function () {

    },
    onShow: function () {
        let userInfo = wx.getStorageSync('userInfo');
        let token = wx.getStorageSync('token');
        let mobile = wx.getStorageSync('mobile');
        console.log(token+"====");
        console.log(mobile+"====");
        var that = this;
        // 页面显示
        if (userInfo && token) {
            app.globalData.userInfo = userInfo;
            app.globalData.token = token;
            if(mobile)
            {
                app.globalData.mobile = mobile;
            }else{
                //获取用户绑定手机信息并保存到全局变量
                user.getBindMobile().then(res => {
                    app.globalData.mobile = wx.getStorageSync('mobile');
                    // _that.setData({
                    //     hasMobile: app.globalData.mobile,
                    // });
                }).catch((e) => {
                    console.log("出现异常！");
                    wx.removeStorageSync('mobile');
                    return;
                });
            }
            that.setData({
                userInfo: app.globalData.userInfo,
                hasMobile: "绑定手机:"+app.globalData.mobile,
            });
        }else{
            that.setData({
                userInfo: app.globalData.userInfoStart,
                hasMobile: '',
            });
        }
    },
    onHide: function () {
        // 页面隐藏

    },
    onUnload: function () {
        // 页面关闭
    },
    bindGetUserInfo(e) {
      let userInfo = wx.getStorageSync('userInfo');
      let token = wx.getStorageSync('token');
      if (userInfo && token) {
        return;
      }
        if (e.detail.userInfo){
            //用户按了允许授权按钮
            var _that = this;
            user.loginByWeixin(e.detail).then(res => {
                _that.setData({
                    userInfo: res.data.userInfo
                });
                app.globalData.userInfo = res.data.userInfo;
                app.globalData.token = res.data.token;

                //获取用户绑定手机信息并保存到全局变量
                user.getBindMobile().then(res => {
                    app.globalData.mobile = wx.getStorageSync('mobile');
                    _that.setData({
                    hasMobile: "绑定手机:"+app.globalData.mobile,
                });
                }).catch((e) => {
                        console.log("出现异常！");
                    wx.removeStorageSync('mobile');
                });


            }).catch((err) => {
                console.log(err)
            });
        } else {
            //用户按了拒绝按钮
            wx.showModal({
                title: '警告通知',
                content: '您点击了拒绝授权,将无法正常显示个人信息,点击确定重新获取授权。',
                success: function (res) {
                    if (res.confirm) {
                        wx.openSetting({
                            success: (res) => {
                                if (res.authSetting["scope.userInfo"]) {////如果用户重新同意了授权登录
                                    user.loginByWeixin(e.detail).then(res => {
                                        this.setData({
                                            userInfo: res.data.userInfo
                                        });
                                        app.globalData.userInfo = res.data.userInfo;
                                        app.globalData.token = res.data.token;

                                        ///获取用户绑定手机信息并保存到全局变量
                                        user.getBindMobile().then(res => {
                                            app.globalData.mobile = wx.getStorageSync('mobile');
                                            _that.setData({
                                                hasMobile: "绑定手机:"+app.globalData.mobile,
                                            });
                                        }).catch((e) => {
                                            console.log("出现异常！");
                                            wx.removeStorageSync('mobile');
                                        });

                                    }).catch((err) => {
                                        console.log(err)
                                    });
                                }
                            }
                        })
                    }
                }
            });
        }
    },
    exitLogin: function () {
        wx.showModal({
            title: '',
            confirmColor: '#b4282d',
            content: '退出登录？',
            success: function (res) {
                if (res.confirm) {
                    wx.removeStorageSync('token');
                    wx.removeStorageSync('userInfo');
                    wx.removeStorageSync('mobile');
                    wx.switchTab({
                        url: '/pages/index/index'
                    });
                }
            }
        })

    },
    toBindMobile:function(){
        console.log(app.globalData.mobile);
        if(app.globalData.mobile!='') {
            wx.showModal({
                title: '',
                content: '已绑定手机号码，确认重新绑定？',
                success: function (res){
                    if (res.confirm) {
                        wx.navigateTo({
                            url: '/pages/auth/mobile/mobile'
                        })
                    }
                }
            });
        }else{
            wx.navigateTo({
                url: '/pages/auth/mobile/mobile'
            });
        }
    }
})