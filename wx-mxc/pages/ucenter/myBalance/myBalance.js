var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var app = getApp();

Page({
  data:{
    balanceList: [],
  },
  onLoad:function(options){

    this.getBalanceList();
  },

    getBalanceList(){
    let that = this;
    util.request(api.BalanceList, {mobile: app.globalData.mobile}, 'GET').then(function (res) {
      console.log(res);
      if (res.errno == 0) {
        console.log(res.data);
        that.setData({
            balanceList: res.data,
        });
      }else if (res.errno == 1 && res.errmsg == "no_bound_mobile"){
          console.log("需要绑定手机后才可以操作!");
          //需要绑定手机后才可以操作
          wx.showModal({
              title: '',
              content: '请先绑定手机',
              success: function (res){
                  if (res.confirm) {
                      wx.removeStorageSync("mobile");
                      wx.navigateTo({
                          url: '/pages/auth/mobile/mobile'
                      })
                  }else if (res.cancel) {
                      //返回上一页
                      wx.navigateBack({
                          delta: 1
                      })
                  }
              }
          });
          return;
      }
      else if (res.errno == 1 && res.errmsg == "query_err"){
          wx.showModal({
              title: '提示',
              content: '未查到余额信息',
              showCancel: false,
              success: function(res) {
                  if (res.confirm) {
                      wx.switchTab({
                          url: '/pages/ucenter/index/index'
                      });
                  }
              }
          })
          return;
      }
    });
  },

  onReady:function(){
    // 页面渲染完成
  },
  onShow:function(){

  },
  onHide:function(){
    // 页面隐藏
  },
  onUnload:function(){
    // 页面关闭
  }
})