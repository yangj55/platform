/**
 * 用户相关服务
 */

const util = require('../utils/util.js');
const api = require('../config/api.js');


/**
 * 调用微信登录
 */
function loginByWeixin(userInfo) {

  let code = null;
  return new Promise(function (resolve, reject) {
    return util.login().then((res) => {
      code = res.code;
      return userInfo;
    }).then((userInfo) => {
      //登录远程服务器
      util.request(api.AuthLoginByWeixin, { code: code, userInfo: userInfo }, 'POST', 'application/json').then(res => {
        if (res.errno === 0) {
          //存储用户信息
          wx.setStorageSync('userInfo', res.data.userInfo);
          wx.setStorageSync('token', res.data.token);

          resolve(res);
        } else {
          util.showErrorToast(res.errmsg)
          reject(res);
        }
      }).catch((err) => {
        reject(err);
      });
    }).catch((err) => {
      reject(err);
    })
  });
}

/**
 * 判断用户是否登录
 */
function checkLogin() {
  return new Promise(function (resolve, reject) {
    if (wx.getStorageSync('userInfo') && wx.getStorageSync('token')) {

      util.checkSession().then(() => {
        resolve(true);
      }).catch(() => {
        reject(false);
      });

    } else {
      reject(false);
    }
  });
}


/**
 * 查找用户绑定手机信息
 */
function getBindMobile() {
    return new Promise(function (resolve, reject) {
        let wx_mobile = wx.getStorageSync('mobile');
        console.log("====aaa"+wx_mobile)
        if (wx_mobile) {
            console.log("====微信本地缓存手机号："+wx_mobile)
            app.globalData.token = wx_mobile;
            resolve(true);
        } else {
            //reject(false);
            //从数据库中查找绑定手机信息
            util.request(api.GetBindMobile, {}, 'GET')
                .then(function (res) {
                    // if (res.data.code == 200) {
                    if (res.errno == 0 && res.data != null && res.data != ''){
                        console.log("====数据库中获得绑定手机号："+res.data);
                        //保存到微信本地缓存
                        wx.setStorageSync('mobile', res.data);
                        //app.globalData.mobile = res.data;
                        resolve(true);
                    }else{
                        reject(false);
                    }
                }).catch(err => {
                reject(false);
        });
        }
    });
}

module.exports = {
    loginByWeixin,
    checkLogin,
    getBindMobile,
};











